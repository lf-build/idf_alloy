FROM node:latest

LABEL RevisionNumber "<<RevisionNumber>>"

CMD ln -s /src/bower_components /src/app/bower_components

RUN apt-get update && apt-get install -y git git-core jq


WORKDIR /src

RUN npm install -g grunt-cli bower git

ADD package.json /src/package.json

RUN npm install

ADD config.js /src/config.js
ADD Gruntfile.js /src/Gruntfile.js
ADD bower.json /src/bower.json
ADD .bowerrc /src/.bowerrc
RUN bower install --allow-root

ADD ./app /src/app

WORKDIR /src

RUN grunt --force build

# Test values for environment variables
# ENV ALLOY_PORTAL test
# ENV ALLOY_DEFAULT_TOKEN 123
# ENV ALLOY_CONFIGURATION_URL //192.168.1.9:5002

ENTRYPOINT \
    sed -i 's~CONFIG_HOST~'${ALLOY_CONFIGURATION_URL}'~g' /src/dist/config.js && \
    sed -i 's~APP_TOKEN~'${ALLOY_DEFAULT_TOKEN}'~g' /src/dist/config.js && \
    sed -i 's~APP_STORE_BASE~'${APP_STORE_BASE}'~g' /src/dist/config.js && \
    find ./dist -type f -exec sed -i 's/CONFIG_PORTAL/'"${ALLOY_PORTAL}"'/g' {} \; && \
    grunt --port=${port} --force dist
