Number.prototype.formatMoney = function (c, d, t) {
  var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

String.prototype.FormateUSAPhone = function () {
  var phone = this;
  if (phone && phone != null) {
    if (phone == undefined) {
      return;
    }
    var mask1;
    try {
      if (phone.indexOf("-") != -1) {
        mask1 = phone.replace(/-/g, "");
      } else {
        mask1 = phone;
      }
      var mask = mask1.match(/^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$/);
      var value = '(' + (mask[1] ? mask[1] : '') + ')-' + (mask[2] ? mask[2] : '') + '-' + (mask[3] ? mask[3] : '');
      return value;
    }
    catch (e) {
      return phone;
    }
  }
  return '';
};