# alloy image creation script
#sh lf_alloy_script.sh dev

docker build --no-cache -t registry.lendfoundry.com/bl-alloy:$1 .

docker push registry.lendfoundry.com/bl-alloy:$1